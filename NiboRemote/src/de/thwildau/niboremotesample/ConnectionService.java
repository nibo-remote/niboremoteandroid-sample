/*
 * Copyright (C) 2013-2014 Tobias Ilte
 *
 * This file is part of NiboRemote.
 *
 * NiboRemote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NiboRemote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NiboRemote. If not, see <http://www.gnu.org/licenses/>.
 */

package de.thwildau.niboremotesample;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;
import de.thwildau.libniboremoteandroid.NiboProtocolParser;

/**
 * This class handles the android-specific part of the bluetooth-data transfer.
 * 
 * @author Tobias Ilte
 * 
 */
public class ConnectionService {
	// Used for Debugging
	private static final String TAG = "Nibo Remote";
	private static final boolean DEBUG = true;

	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothSocket mmSocket;
	private BluetoothDevice mmDevice;

	private InputStream mmInStream;
	private OutputStream mmOutStream;

	private ConnectThread mConnectThread;


	private NiboProtocolParser mNiboProtocolParser;

	// common UUID for serial devices
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private final Handler mHandler;

	public ConnectionService(String deviceAddress, Handler handler, NiboProtocolParser parser) {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mmDevice = mBluetoothAdapter.getRemoteDevice(deviceAddress);
		mHandler = handler;
		mNiboProtocolParser = parser;
	}

	public void connect() {
		mConnectThread = new ConnectThread();
		mConnectThread.start();
	}

	/**
	 * Calls the relevant functions to build the request string, send it and
	 * receive/parse the answer.
	 */
	public void submitData() {
		String requestString = mNiboProtocolParser.buildRequest();
		if (DEBUG) Log.d(TAG, "about to write: " + requestString);
		write(requestString.getBytes());
		if (DEBUG) Log.d(TAG, "Data sent");
		byte[] reply = read();
		if (DEBUG) Log.d(TAG, "Reply received");

		// count the number of bytes in the reply-array
		int counter = 0;
		for (int i = 0; i < reply.length; i++) {
			if (reply[i] != 0) {
				counter++;
			}
		}
		mNiboProtocolParser.parseAnswer(new String(reply, 0, counter));
	}

	/**
	 * Write to the ConnectedThread
	 * 
	 * @param out
	 *            The String to write
	 * @see ConnectedThread#write(byte[])
	 */
	public void write(byte[] bytes) {
		try {
			mmOutStream.write(bytes);
			if (DEBUG) Log.d(TAG, "data sent!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read incoming data.
	 * 
	 * @param in
	 *            The array, where reply is stored
	 * @see ConnectedThread#read(byte[])
	 */
	public byte[] read() {
		byte[] buffer = new byte[512];

		int bytes = 0;
		if (DEBUG) Log.d(TAG, "waiting for receiving data...");

		int timer = 0;
		boolean fail = false;
		try {
			int available = 0;

			while (true) {
				available = mmInStream.available();
				if (available > 0) {
					break;
				}
				Thread.sleep(1);
				timer++;
				if (timer > 1000) {
					fail = true;
					Log.e(TAG, "timeout");
					break;
				}
			}

			Log.i(TAG, "available bits: " + available);
			if (!fail) {
				do {

					// Read from the InputStream
					try {
						bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
						if (DEBUG)
							Log.d(TAG, "number of bytes received:" + Integer.toString(bytes));
					} catch (IOException e) {
						e.printStackTrace();
						// mHandler.obtainMessage(ConnectedActivity.MESSAGE_CONNECTION_READY,
						// 1, -1).sendToTarget();
					}

				} while (buffer[bytes - 1] != 10);
			}
			Log.i(TAG, "available bits: " + mmInStream.available());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (DEBUG) Log.d(TAG, "all data reveived!");

		return buffer;
	}

	private class ConnectThread extends Thread {

		public ConnectThread() {

			// Get a BluetoothSocket to connect with the given BluetoothDevice
			try {
				mmSocket = mmDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (DEBUG) Log.d(TAG, "Socket created");
		}

		public void run() {
			try {
				// Connect the device through the socket. This will block
				// until it succeeds or throws an exception
				mmSocket.connect();
			} catch (IOException connectException) {
				// Unable to connect; close the socket and get out
				connectException.printStackTrace();
				try {
					mmSocket.close();
					mHandler.obtainMessage(ConnectedActivity.MESSAGE_CONNECTION_FAIL, 1, -1)
							.sendToTarget();
				} catch (IOException closeException) {
					closeException.printStackTrace();
				}
				return;
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (DEBUG) Log.d(TAG, "Socket connected");

			try {
				mmInStream = mmSocket.getInputStream();
				mmOutStream = mmSocket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (DEBUG) Log.d(TAG, "IO-Streams ready");

			mHandler.obtainMessage(ConnectedActivity.MESSAGE_CONNECTION_READY, 1, -1)
					.sendToTarget();

			if (mConnectThread != null) {
				mConnectThread = null;
			}

		}

		/** Will cancel an in-progress connection, and close the socket */
		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
			}
		}
	}

	public void stop() {
		if (DEBUG) Log.d(TAG, "stop");
		try {
			mmSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
	}
}
