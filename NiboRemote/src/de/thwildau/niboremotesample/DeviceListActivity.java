/*
 * Copyright (C) 2013-2014 Tobias Ilte
 *
 * This file is part of NiboRemote.
 *
 * NiboRemote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NiboRemote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NiboRemote. If not, see <http://www.gnu.org/licenses/>.
 */

package de.thwildau.niboremotesample;

import java.util.ArrayList;
import java.util.Set;

import de.thwildau.niboremote.R;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This Activity gets called on startup of the application and lists all possible bluetooth devices
 * to connect to.
 * 
 * @author Tobias Ilte
 * @version 1.0
 */
public class DeviceListActivity extends Activity {
	// Used for Debugging
	private static final String TAG = "DeviceListActivity";
	private static final boolean DEBUG = true;

	// Return Intent extra
	public static final String EXTRA_DEVICE_ADDRESS = "device_address";
	private final int REQUEST_ENABLE_BT = 1;

	private BluetoothAdapter mBluetoothAdapter;
	private ArrayList<String> bluetoothDeviceList;
	private ArrayAdapter<String> mBtDevicesArrayAdapter;
	private ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		bluetoothDeviceList = new ArrayList<String>();
		mBtDevicesArrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, bluetoothDeviceList);
		lv = (ListView) findViewById(R.id.listView_available_devices);
		lv.setAdapter(mBtDevicesArrayAdapter);
		lv.setOnItemClickListener(mDeviceClickListener);
		bluetoothEnable();
		listDevices();
	}

	/** If bluetooth is disabled, asks to enable it on the device. */
	private void bluetoothEnable() {
		// Close Application, if Device does not support bluetooth.
		if (mBluetoothAdapter == null) {
			finish();
		}
		// if bluetooth is disabled, ask to enable it
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
	}

	/** Lists paired devices. */
	private void listDevices() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

		// If there are paired devices
		if (pairedDevices.size() > 0) {
			// Loop through paired devices
			for (BluetoothDevice device : pairedDevices) {
				// Add the name and address to an array adapter to show in a
				// ListView
				mBtDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
			}
		}
	}

	/**
	 * Defines a receiver for receiving events regarding the discovery of new devices.
	 */
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice newDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String newDeviceAddress = newDevice.getAddress();
				boolean alreadyExists = false;
				for (int i = 0; i < mBtDevicesArrayAdapter.getCount(); i++) {
					String deviceString = mBtDevicesArrayAdapter.getItem(i);
					// Get the device MAC address, which is the last 17 chars in the View
					String address = deviceString.substring(deviceString.length() - 17);
					if (address.equals(newDeviceAddress)) {
						alreadyExists = true;
						break;
					}
				}

				if (!alreadyExists) {
					// Add the name and address to an array adapter to show in a ListView
					mBtDevicesArrayAdapter.add(newDevice.getName() + "\n" + newDeviceAddress);
				}
				// When discovery is finished.
			} else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				setProgressBarIndeterminateVisibility(false);
				setTitle(R.string.select_device);
			}
		}
	};

	/**
	 * Start device discover with the BluetoothAdapter when the relevant button is pressed.
	 */
	public void doDiscovery(View view) {
		Button mButton = (Button) findViewById(R.id.button_searching);
		// If we're already discovering, stop it
		if (mBluetoothAdapter.isDiscovering()) {
			mButton.setText(getResources().getString(R.string.button_searching));
			mBluetoothAdapter.cancelDiscovery();
			setProgressBarIndeterminateVisibility(false);
			setTitle(R.string.select_device);
		} else {

			mButton.setText(getResources().getString(R.string.button_searching_stop));
			if (DEBUG) Log.d(TAG, "doDiscovery()");

			// Register the BroadcastReceiver when new device is discovered
			IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
			this.registerReceiver(mReceiver, filter);

			// Register for broadcasts when discovery has finished
			filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
			this.registerReceiver(mReceiver, filter);

			// Indicate scanning in the title
			setProgressBarIndeterminateVisibility(true);
			setTitle(R.string.scanning);

			// Turn on sub-title for new devices
			findViewById(R.id.title_available_devices).setVisibility(View.VISIBLE);

			// Request discover from BluetoothAdapter
			mBluetoothAdapter.startDiscovery();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Make sure we're not doing discovery anymore
		if (mBluetoothAdapter != null) {
			mBluetoothAdapter.cancelDiscovery();
		}
		try {
			// Unregister broadcast listeners
			this.unregisterReceiver(mReceiver);
		} catch (IllegalArgumentException e) {
			Log.i(TAG, "Receiver does not exists. Nothing to unregister...");
		}

	}

	/** The on-click listener for all devices in the ListViews */
	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
			// Cancel discovery because it's costly and we're about to connect
			if (mBluetoothAdapter.isDiscovering() == true) {
				mBluetoothAdapter.cancelDiscovery();
			}

			// Get the device MAC address, which is the last 17 chars in the
			// View
			String info = ((TextView) v).getText().toString();
			String address = info.substring(info.length() - 17);

			// Create the result Intent and include the MAC address
			Intent intent = new Intent(DeviceListActivity.this, ConnectedActivity.class);
			intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
			startActivity(intent);
			finish();
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_OK) {
				listDevices();
			} else if (resultCode == RESULT_CANCELED) {
				finish();
			}
		}
	}
}