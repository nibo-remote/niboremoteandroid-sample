/*
 * Copyright (C) 2013-2014 Tobias Ilte
 *
 * This file is part of NiboRemote.
 *
 * NiboRemote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NiboRemote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NiboRemote. If not, see <http://www.gnu.org/licenses/>.
 */

package de.thwildau.niboremotesample;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
//import de.thwildau.libniboremoteandroid.NiboProtocolHelper;
import de.thwildau.libniboremoteandroid.NiboProtocolParser;
import de.thwildau.niboremote.R;

/**
 * This activity shows sensor data of the robot and offers the possibility to update them.
 * 
 * @author Tobias Ilte
 * 
 */
public class ConnectedActivity extends Activity {
	// Used for Debugging
	private static final String TAG = "Nibo Remote";
	private static final boolean DEBUG = true;

	// Messages for the handler
	public static final int MESSAGE_CONNECTION_READY = 1;
	public static final int MESSAGE_CONNECTION_FAIL = 2;

	private ConnectionService mConnectionService;
	private NiboProtocolParser mNiboProtocolParser;
//	private NiboProtocolHelper mNiboProtocolHelper;
	private ProgressDialog connectDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connected);
		if (DEBUG) Log.d(TAG, "ConnectedActivity created");
		connectDialog = ProgressDialog.show(this, "Please Wait", "Connecting to device...", true, false);
		Intent intent = getIntent();
		// get device address from the device list activity
		String deviceAddress = intent.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
		mNiboProtocolParser = new NiboProtocolParser(32);
//		mNiboProtocolHelper = new NiboProtocolHelper(mNiboProtocolParser);
		mConnectionService = new ConnectionService(deviceAddress, new ConnectionHandler(this), mNiboProtocolParser);
		/** Establish a connection to remote device and create nibo-register. */
		mConnectionService.connect();
	}


	/** On Button click request new sensor data and show them. */
	public void updateSensors(View view) {
		new TransmitDataTask().execute();
	}

	private class TransmitDataTask extends AsyncTask<Void, Void, Void> {
		protected void onPreExecute(){
			mNiboProtocolParser.registerFlags[19] = NiboProtocolParser.NSP_FLAG_GET;
			mNiboProtocolParser.registerFlags[20] = NiboProtocolParser.NSP_FLAG_GET;
			mNiboProtocolParser.registerFlags[21] = NiboProtocolParser.NSP_FLAG_GET;
			mNiboProtocolParser.registerFlags[22] = NiboProtocolParser.NSP_FLAG_GET;
			mNiboProtocolParser.registerFlags[23] = NiboProtocolParser.NSP_FLAG_GET;
			mNiboProtocolParser.registerFlags[24] = NiboProtocolParser.NSP_FLAG_GET;
		}
		
	    /** The system calls this to perform work in a worker thread and
	      * delivers it the parameters given to AsyncTask.execute() */
	    protected Void doInBackground(Void... params) {
	    	mConnectionService.submitData();
	    	return null;
	    }
	    
	    /** The system calls this to perform work in the UI thread and delivers
	      * the result from doInBackground() */
	    protected void onPostExecute(Void v) {
	    	EditText editTextL = (EditText) findViewById(R.id.editTextDistL);
			EditText editTextFL = (EditText) findViewById(R.id.editTextDistFL);
			EditText editTextF = (EditText) findViewById(R.id.editTextDistF);
			EditText editTextFR = (EditText) findViewById(R.id.editTextDistFR);
			EditText editTextR = (EditText) findViewById(R.id.editTextDistR);
			EditText editTextNDS3 = (EditText) findViewById(R.id.editTextDistNDS3);
			editTextL.setText(Integer.toString(mNiboProtocolParser.registerValues[19]));
			editTextFL.setText(Integer.toString(mNiboProtocolParser.registerValues[20]));
			editTextF.setText(Integer.toString(mNiboProtocolParser.registerValues[21]));
			editTextFR.setText(Integer.toString(mNiboProtocolParser.registerValues[22]));
			editTextR.setText(Integer.toString(mNiboProtocolParser.registerValues[23]));
			editTextNDS3.setText(Integer.toString(mNiboProtocolParser.registerValues[24]));
	    }
	}
	
	
	
	
	
	
	/** Inner Handler-Class which deals with messages from other threads. */
	static class ConnectionHandler extends Handler {
		// reference to parent activity
		private final WeakReference<ConnectedActivity> mConnectedActivity;

		ConnectionHandler(ConnectedActivity connectedActivity) {
			mConnectedActivity = new WeakReference<ConnectedActivity>(connectedActivity);
		}

		@Override
		public void handleMessage(Message msg) {
			final ConnectedActivity parentActivity = mConnectedActivity.get();
			switch (msg.what) {
				case MESSAGE_CONNECTION_READY:
					if (msg.arg1 == 1) {
						parentActivity.connectDialog.dismiss();
					}
					break;
				case MESSAGE_CONNECTION_FAIL:
					if (msg.arg1 == 1) {
						parentActivity.connectDialog.dismiss();
						AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
						builder.setMessage(R.string.alertdialog_message);
						builder.setCancelable(false);

						// Add the buttons
						builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								parentActivity.connectDialog = ProgressDialog.show(parentActivity, "Please Wait",
										"Connecting to device...", true, false);
								parentActivity.mConnectionService.connect();
							}
						});
						builder.setNegativeButton(R.string.quit, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								parentActivity.finish();
							}
						});

						// Create the AlertDialog
						AlertDialog dialog = builder.create();
						dialog.show();
					}
			}
		}
	};

	@Override
	public void onStop() {
		super.onStop();
		if (DEBUG) Log.d(TAG, "-- ON STOP --");
		mConnectionService.stop();
	}
}